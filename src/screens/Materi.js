import React, { useState } from 'react';
import {Text, View, Image, TouchableOpacity, ScrollView} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Symbol from 'react-native-vector-icons/Fontisto';
import Symbol2 from 'react-native-vector-icons/MaterialCommunityIcons';
import styles from '../components/style';
import { Picker } from '@react-native-community/picker';

function Materi ({navigation}) {
  const [tipe, setTipe] = useState('')
  return (
    <View style={styles.container}>
      <View style={styles.navigationbar}>
        <TouchableOpacity>
        <Icon name = 'arrow-back' size={25}/>
        </TouchableOpacity>
        <Text style={styles.textjudul}>Materi</Text>
        <TouchableOpacity>
          <Icon name = 'search' size={25} />
        </TouchableOpacity>
      </View>
      
      <View style={styles.containerUser}>
        <Image source={require('../assets/images/fotoprofile.png')} style={styles.fotoProfile}/>
        <View style={styles.userInfo}>
          <Text style={styles.text}>Hi, Sintya Marisca</Text>
          <Text style={styles.tipeUser}>Free User</Text>
          <View style={styles.profileInfo}>
          <Symbol name = 'wallet' size={15} color='blue'/>
          <Text style={styles.money}>Rp0</Text>
          <Symbol2 name = 'diamond' size={18} color='orange'/>
          <Text style={styles.diamond}>5</Text>
          </View> 
        </View>
        <TouchableOpacity style={styles.containerUpgrade}>
            <Text style={styles.textUpgrade}>Upgrade {'\n'} ke Premium</Text>
        </TouchableOpacity>
      </View>

      <View style={styles.containerMateriPelajaran}>
        <View style={styles.containerJudul}>
        <Text style={styles.textjudul}>Materi Pelajaran</Text>
        <View style={styles.tipeMateri}>
          <Picker
            selectedValue={tipe}
            style={{height:25, width: 125, borderRadius: 5, fontSize: 12, color: 'white', fontWeight: 'bold'}}
            itemStyle={{justifyContent:'flex-start', fontSize: 12, backgroundColor: '#8ED49A' }}
            mode='dropdown'
            onValueChange={(tipe) => setTipe(tipe)}
            >
              <Picker.Item label= 'SKD' value='SKD'/>
              <Picker.Item label= 'SKB' value='SKB'/>
          </Picker>
          {/* <Text style={styles.textTipeMateri}>SKD</Text> */}
        </View>
        </View>

        <View style={styles.containerIconMateri}>
          <View style=
          {styles.LineIconMateri}>
            <TouchableOpacity style={styles.IconMateri}>
              <Image source={require('../assets/images/wawasan.png')}/>
              <Text style={styles.textIconMateriPelajaran}>Wawasan{'\n'}Kebangsaan</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.IconMateri}>
              <Image source={require('../assets/images/intelegensi.png')} />
              <Text style={styles.textIconMateriPelajaran}>Intelegensi {'\n'}Umum</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.IconMateri}><Image source={require('../assets/images/intelegensi.png')} />
            <Text style={styles.textIconMateriPelajaran}>Karakteristik{'\n'}Pribadi</Text>
            </TouchableOpacity>
          </View>
          <View style=
          {styles.LineIconMateri}>
            <TouchableOpacity style={styles.IconMateri}><Image source={require('../assets/images/figural.png')} />
            <Text style={styles.textIconMateriPelajaran}>Figural</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.IconMateri}><Image source={require('../assets/images/logika.png')} />
            <Text style={styles.textIconMateriPelajaran}>Logika</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.IconMateri}><Image source={require('../assets/images/spasial.png')} />
            <Text style={styles.textIconMateriPelajaran}>Spasial</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>

      <View style={styles.containerJudul2}>
      <Text style={styles.textjudul}>Terakhir Dipelajari</Text>
      <TouchableOpacity>
      <Text style={styles.textLihatLainnya}>Lihat Lainnya</Text>
      </TouchableOpacity>
      </View>
      <View style={styles.containermateriTerakhir}>
      <ScrollView 
        horizontal={true}
        showsHorizontalScrollIndicator={false}
      >
        <TouchableOpacity style={styles.materi}
        onPress={()=>navigation.navigate('LatihanSoal')}>
          <View style={styles.infoMateri}>
            <Text style={styles.text}>Medan Magnet</Text>
            <Text style={styles.keterangan}> Video - 13 jam lalu</Text>
          </View>
          <Image style={styles.imageMateri} source={require('../assets/images/medanmagnet.png')}/>
        </TouchableOpacity>
        <TouchableOpacity style={styles.materi2}>
          <View style={styles.infoMateri}>
            <Text style={styles.text}>Simple Past Tense</Text>
            <Text style={styles.keterangan}> E-book - 16 jam lalu</Text>
          </View>
          <Image style={styles.imageMateri} source={require('../assets/images/medanmagnet.png')}/>
        </TouchableOpacity>
      </ScrollView>
      </View>
      
      <View style={styles.containerSBMPTN}>
        <Text style={styles.textjudul}>Survey SBMPTN</Text>
        <TouchableOpacity>
        <Image style={styles.imageSurvey} source={require('../assets/images/diskonsurvey.png')} />
        </TouchableOpacity>
      </View>
      
    </View>
  )
}

export default Materi;