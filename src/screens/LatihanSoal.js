import React from 'react';
import {View, Text, TouchableOpacity, ScrollView, Image} from 'react-native';
import styles from '../components/style';
import Icon from 'react-native-vector-icons/Ionicons';
import Symbol2 from 'react-native-vector-icons/MaterialCommunityIcons';
import style from '../components/style';

function LatihanSoal ({navigation}) {
  return (
    <View style={styles.container}>
       <View style={styles.navigationbar}>
        <TouchableOpacity
          onPress={() => navigation.navigate('Materi')}>
        <Icon name = 'arrow-back' size={25}/>
        </TouchableOpacity>
        <TouchableOpacity style={styles.downloadSoal}>
          <Icon name ='download' size={18} color={'white'}/>
          <Text style={styles.textDownload}>Download Soal
          </Text>
        </TouchableOpacity>
        </View>

        <View style={styles.containerJudulLat}>
          <View style={styles.barLatihanSoal}>
          <Text style={styles.textjudul}>Latihan Soal 3</Text>
          <Text style={styles.textMateriFisika}>Fisika Medan Magnet</Text>
          </View>
          <Image source={require('../assets/images/logo.png')} />
        </View>

        <View style={styles.containerNomor}>
          <View style={styles.barjudulNomor}>
            <Text>Nomor Soal</Text>
            <TouchableOpacity>
              <Text style={styles.keterangan}>Lihat Semua 
                <Symbol2 name = 'chevron-right' size={15}/>
              </Text>
            </TouchableOpacity>
          </View>
          <View style={styles.barNomor}>
          <ScrollView 
            horizontal={true}
            showsHorizontalScrollIndicator={false}>
            <TouchableOpacity style={styles.elipsNomor1}>
              <Text>1</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.elipsNomor2}>
              <Text style={styles.textnomor}>2</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.elipsNomor2}>
              <Text style={styles.textnomor}>3</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.elipsNomor4}>
              <Text style={styles.textnomor}>4</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.elipsNomor5}>
              <Text style={styles.textnomor}>5</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.elipsNomor5}>
              <Text style={styles.textnomor}>6</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.elipsNomor2}>
              <Text style={styles.textnomor}>7</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.elipsNomor5}>
              <Text style={styles.textnomor}>8</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.elipsNomor5}>
              <Text style={styles.textnomor}>9</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.elipsNomor2}>
              <Text style={styles.textnomor}>10</Text>
            </TouchableOpacity>
          </ScrollView>  
          </View>
        </View>

        <ScrollView style={styles.containerSoal}>
          <View style={styles.barInfoSoal}>
            <Text> Info Soal No.1</Text>
            <Icon name = 'chevron-down' size={30} color={'#2FB44B'}/>
          </View>
          <Image source={require('../assets/images/soal.png')} />
          <Text style={styles.textJawaban}>Jawaban kamu benar</Text>
          <View style={styles.pilihanGandaSelection}>
            <View style={styles.elipsPilGan}>
              <Text style={styles.textnomor}>a</Text>
            </View>
            <Text style={styles.jawabanPilGan}>678 m ³</Text>
          </View>
          <View style={styles.pilihanGanda}>
          <View style={styles.elipsPilGan2}>
            <Text style={styles.pilgan}>b</Text>
            </View>
            <Text style={styles.jawabanPilGan2}>678 m ³</Text>
          </View>
          <View style={styles.pilihanGanda}>
          <View style={styles.elipsPilGan2}>
            <Text style={styles.pilgan}>c</Text>
            </View>
            <Text style={styles.jawabanPilGan2}>678 m ³</Text>
          </View>
          <View style={styles.pilihanGanda}>
          <View style={styles.elipsPilGan2}>
            <Text style={styles.pilgan}>d</Text>
            </View>
            <Text style={styles.jawabanPilGan2}>678 m ³</Text>
          </View>
          <View style={styles.pilihanGanda}>
          <View style={styles.elipsPilGan2}>
            <Text style={styles.pilgan}>e</Text>
            </View>
            <Text style={styles.jawabanPilGan2}>678 m ³</Text>
          </View>

          <TouchableOpacity style={styles.lihatPembahasan}>
            <Text style={styles.textLihatPembahasan}>
              <Symbol2 name = 'lock' size={18} color={'white'}/>
              Lihat Pembahasan</Text>
          </TouchableOpacity>

          <View style={styles.barSlides}>
          <TouchableOpacity style={styles.slides}>
            <Symbol2 name = 'chevron-left' size={50} color={'white'}/>
          </TouchableOpacity>
          <TouchableOpacity style={styles.slides2}>
            <Symbol2 name = 'chevron-right' size={50} color={'white'}/>
          </TouchableOpacity>
          </View>
        </ScrollView>
      <View style={styles.tabBar}>
      <TouchableOpacity style={styles.lihatVideoPembahasan}>
            <Text style={styles.textLihatVideoPembahasan}>
              <Symbol2 name = 'lock' size={18} color={'grey'}/>
              Lihat Video Pembahasan</Text>
          </TouchableOpacity>
      </View>
    </View>
  )
}

export default LatihanSoal;