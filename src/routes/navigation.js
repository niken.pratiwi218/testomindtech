import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Materi from '../screens/Materi';
import LatihanSoal from '../screens/LatihanSoal'

const RootStack = createStackNavigator();

export default class Navigation extends React.Component{
  render(){
    return(
      <NavigationContainer>
        <RootStack.Navigator initialRouteName='Materi' headerMode='none'>
          <RootStack.Screen
          name='Materi'
          component={Materi}
          />
          <RootStack.Screen
          name='LatihanSoal'
          component={LatihanSoal}
          />
        </RootStack.Navigator>
      </NavigationContainer>
    )
  }
}