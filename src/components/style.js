import React from 'react';
import { StyleSheet} from "react-native";

const style=StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#EBF6E9',
    paddingVertical: 10
  },
  navigationbar: {
    height: 40,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 15,
  },
  textjudul: {
    fontSize: 18,
    fontWeight: 'bold'
  },
  containerUser: {
    height: '12%',
    width: '95%',
    alignSelf: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    marginVertical: 10,
    borderRadius: 10,
    flexDirection: 'row',
    paddingHorizontal: 15
  },
  fotoProfile:{
    height: 60,
    width: 60,
    borderRadius: 60,
    marginRight: 15,
  },
  profileInfo : {
    height: 30,
    width: 90,
    flexDirection: 'row',
    padding: 5,
    justifyContent: 'space-between'
  },
  text: {
    fontSize: 15,
    fontWeight: 'bold'
  },
  tipeUser: {
    color: 'orange'
  },
  money: {
    color: 'blue'
  },
  diamond: {
    color: 'orange'
  },
  containerUpgrade: {
    width: 80,
    height: 40,
    borderRadius: 5,
    backgroundColor: '#FDC416',
    justifyContent: 'center',
    marginLeft: 80
  },
  textUpgrade: {
    textAlign: "center",
    fontWeight: 'bold',
    fontSize: 10
  },
  containerMateriPelajaran: {
    height: '35%',
    width: '95%',
    alignSelf: 'center',
    marginVertical: 10
  },
  containerJudul: {
    height: 25,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
  }, 
  containerJudul2: {
    height: 25,
    width: '95%',
    flexDirection: 'row',
    alignSelf: 'center',
    justifyContent: 'space-between',
    alignItems: 'center',
    margin: 10
  },
  tipeMateri: {
    height: 25,
    width: 125,
    borderRadius: 5,
    backgroundColor: '#8ED49A',
    justifyContent: 'center',
    padding: 5
  },
  textTipeMateri:{
    fontWeight: 'bold',
    color: 'white'
  },
  containerIconMateri: {
    backgroundColor: 'white',
    width: '100%',
    height: '85%',
    borderRadius: 5,
    marginTop: 10
  },
  LineIconMateri: {
    height:'50%',
    width: '100%',
    justifyContent: 'space-evenly',
    flexDirection: 'row'
  },
  IconMateri: {
    width: 100,
    height: '100%',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  textIconMateriPelajaran: {
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 12
  },
  textLihatLainnya:{
    color: '#8ED49A',
    fontWeight: 'bold'
  },
  containermateriTerakhir: {
    height:'11%',
    flexDirection: 'row',
    paddingLeft: 10,
    marginVertical: 10
  },
  materi: {
    width: 220,
    height:'100%',
    backgroundColor: '#FFF8BB',
    borderRadius: 5,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    marginHorizontal: 10
  },
  materi2: {
    width: 220,
    height:'100%',
    backgroundColor: '#FFD2B9',
    borderRadius: 5,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    marginHorizontal: 10
  },
  infoMateri:{
    width: '65%',
    height: '100%',
    flexDirection: 'column',
    justifyContent: 'center',
    marginLeft: 10
  },
  imageMateri: {
    height: 40,
    width: 40
  },
  keterangan: {
    fontSize: 12,
    color: 'grey'
  },
  containerSBMPTN:{
    width: '95%',
    height:'20%',
    alignSelf: 'center',
    marginVertical: 10
  },
  imageSurvey: {
    width: '100%',
    height:'90%',
    resizeMode: 'contain'
  },
  downloadSoal: {
    height: 30,
    width: 140,
    backgroundColor: '#2FB44B',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    borderRadius: 5,
    flexDirection: 'row'
  },
  textDownload: {
    fontSize: 14,
    color: 'white'
  },
  containerJudulLat:{
    height: 150,
    width: '95%',
    alignSelf: 'center',
    flexDirection: "row",
    justifyContent: 'center',
    alignItems: 'center'
  },
  barLatihanSoal: {
    height: '100%',
    width: '65%',
    justifyContent:'center'
  },
  textMateriFisika:{
    color: '#2FB44B',
    fontSize: 16,
    fontWeight: 'bold',
    marginTop: 5
  },
  containerNomor:{
    height: 70,
    width: '85%',
    alignSelf: "center",
    backgroundColor: 'white',
    borderRadius: 5,
    flexDirection: 'column',
    elevation: 5
  },
  barjudulNomor: {
    width: '100%',
    height: '35%',
    justifyContent: 'space-between',
    flexDirection: "row",
    padding: 5
  },
  barNomor:{
    height: '65%',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    padding: 3
  },
  elipsNomor1: {
    height: 30,
    width: 30,
    borderRadius: 30,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    marginHorizontal: 3,
    elevation: 5
  },
  elipsNomor2: {
    height: 30,
    width: 30,
    borderRadius: 30,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#8ED49A',
    marginHorizontal: 3
  },
  textnomor: {
    color: 'white'
  },
  elipsNomor4: {
    height: 30,
    width: 30,
    borderRadius: 30,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#B7B7B7',
    marginHorizontal: 3
  },
  elipsNomor5: {
    height: 30,
    width: 30,
    borderRadius: 30,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#EF827F',
    marginHorizontal: 3
  },
  containerSoal:{
    width: '95%',
    backgroundColor: 'white',
    borderRadius: 5,
    alignSelf: 'center',
    marginTop: -25,
    paddingTop: 30,
    flexDirection: 'column',
    paddingHorizontal: 10,
  },
  barInfoSoal: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  textJawaban:{
    fontWeight: 'bold',
    color:"#2FB44B",
    fontSize: 15
  },
  pilihanGandaSelection:{
    height: 40,
    width: '98%',
    backgroundColor:'#C8E3BE',
    borderRadius: 5,
    borderColor: '#8ED49A',
    borderWidth: 3,
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 10
  },
  pilihanGanda:{
    height: 40,
    width: '98%',
    backgroundColor:'white',
    elevation: 5,
    marginVertical: 10,
    flexDirection: 'row',
    alignItems: 'center',
  },
  elipsPilGan:{
    height: 25,
    width: 25,
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#2FB44B',
    marginHorizontal: 20
  },
  jawabanPilGan:{
    fontSize: 15,
    fontWeight: 'bold'
  },
  elipsPilGan2:{
    height: 25,
    width: 25,
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    marginHorizontal: 20
  },
  jawabanPilGan2:{
    fontSize: 15,
    fontWeight: 'bold'
  },
  pilgan:{
    color: 'black'
  },
  lihatPembahasan:{
    height: 35,
    width: '100%',
    backgroundColor :'#2FB44B',
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 15
  },
  textLihatPembahasan: {
    fontSize: 16,
    color: 'white',
    fontWeight: 'bold'
  },
  slides: {
    height: 50,
    width: 50,
    borderRadius: 50,
    backgroundColor: '#DDDDDD',
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    margin: 5
  },
  barSlides: {
    flexDirection: 'row',
    justifyContent: 'center',    
    paddingBottom: 30
  },
  slides2: {
    height: 50,
    width: 50,
    borderRadius: 50,
    backgroundColor: '#2FB44B',
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    margin: 5
  },
  lihatVideoPembahasan:{
    height: 35,
    width: '90%',
    backgroundColor :'#DDDDDD',
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    marginVertical: 15
  },
  textLihatVideoPembahasan: {
    fontSize: 16,
    color: 'grey',
    fontWeight: 'bold'
  },
})

export default style;